<?php

declare(strict_types=1);

namespace Api\Http\Resources;

use LenderKit\Api\Http\Resources\Resource as LenderKitResource;

/**
 * Class Resource
 *
 * @package Api\Http\Resources
 */
abstract class Resource extends LenderKitResource
{

}
