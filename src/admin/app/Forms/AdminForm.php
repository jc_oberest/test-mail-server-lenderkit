<?php

declare(strict_types=1);

namespace Admin\Forms;

use LenderKit\Admin\Forms\AdminForm as LenderKitAdminForm;

/**
 * Class Form
 *
 * @package Admin\Forms
 */
abstract class AdminForm extends LenderKitAdminForm
{

}
